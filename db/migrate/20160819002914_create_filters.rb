class CreateFilters < ActiveRecord::Migration
  def change
    create_table :filters do |t|
      t.string :keywords
      t.string :category
      t.integer :view

      t.timestamps null: false
    end
  end
end
