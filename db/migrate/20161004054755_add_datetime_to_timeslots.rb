class AddDatetimeToTimeslots < ActiveRecord::Migration
  def change
  	add_column :time_slots, :start_date, :datetime
  	add_column :time_slots, :end_date, :datetime
  end
end
