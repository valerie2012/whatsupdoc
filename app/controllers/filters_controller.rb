class FiltersController < ApplicationController

	def new
		@filter = Filter.new
		@in_network_insurance = Filter.uniq.pluck(:in_network_insurance)
		@city = Filter.uniq.pluck(:city)
	end

	def create
		@filter = Filter.create(search_params)
		redirect_to @search
	end

	def show
		@filter = Filter.find(params[:id])
	end

	private

	def search_params
		params.require(:filter).permit(:specialty, :in_network_insurance, :city, :first_name, :last_name)
	end
	
end