class Appointment < ActiveRecord::Base
	belongs_to :doctor

	RailsAdmin.config do |config|
		config.model 'Appointment' do
			navigation_icon 'fa fa-calendar-o'
			

			create do
				field :status, :enum do 
					enum do 
						['Pending','Cancelled','Confirmed']
					end
				end
				field :patient_name
				field :age
				field :gender
				field :insurance
				field :reason_for_visit
				field :doctor
				field :new_patient
				field :date_of_appointment
				field :time_slot_id
			end

			edit do
				field :status, :enum do 
					enum do 
						['Pending','Cancelled','Confirmed']
					end
				end
				field :patient_name
				field :age
				field :gender
				field :insurance
				field :reason_for_visit
				field :doctor
				field :new_patient
				field :date_of_appointment
				field :time_slot_id
				
			end

			list do
				field :status
				field :patient_name
				field :insurance
				field :reason_for_visit
				field :doctor
				field :new_patient
				field :date_of_appointment
				field :time_slot_id
				
			end



		end

	end
end
