class Search < ActiveRecord::Base

	def search_doctors
		doctors = Doctor.where( status: 'true')

		doctors = doctors.where(["first_name LIKE ?", first_name]) if first_name.present?
		doctors = doctors.where(["specialty LIKE ?", specialty]) if specialty.present?
		doctors = doctors.where(["city LIKE ?", city])if city.present?
		doctors = doctors.where(["in_network_insurance LIKE ?", in_network_insurance])if in_network_insurance.present?

		return doctors

	end

end
