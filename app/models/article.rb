class Article < ActiveRecord::Base
	belongs_to :category

	has_attached_file :article_img, styles: { medium: "300x300>", thumb: "100x100>" }, default_url: "/images/:style/missing.png" 
	validates_attachment_content_type :article_img, content_type: /\Aimage\/.*\Z/

	def self.search_articles(filter)
		if filter
			where(["title LIKE ?", "%#{filter}%"])
			# where(["created_at LIKE ?", date]) if created_at.present?
		    # where(["category_id LIKE ?", category_id]) if category_id.present?
			# where(["view ?", view])if view.present?
		else
			all.order('created_at DESC')
		end
	end

	# def search_articles
	# 	articles = articles.all.order('created_at DESC')
	# 	articles = articles.where(["created_at LIKE ?", date]) if created_at.present?
	# 	articles = articles.where(["category_id LIKE ?", category])if category_id.present?
	# 	articles = articles.where(["view ?", view])if view.present?

	# 	return articles
	# end

	RailsAdmin.config do |config|
		config.model 'Article' do

			# Create fields
			create do 
				field :article_img 
				field :title
				field :description
				field :category_id
				field :created_at
				field :updated_at
				
			end

			# Edit fields
			edit do 
				field :article_img
				field :title
				field :description
				field :category_id
				field :created_at
				field :updated_at
			
			end

			# Show fields
			list do 
				field :article_img
				field :title
				field :description
				field :category_id
				field :created_at
				field :updated_at
				
			end
		end
	end

end
