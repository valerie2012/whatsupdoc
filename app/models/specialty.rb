class Specialty < ActiveRecord::Base
	belongs_to :doctors

	RailsAdmin.config do |config|
		config.model 'Specialty' do
			
			create do
				field :name
				field :description
			end
		end
	end
end
