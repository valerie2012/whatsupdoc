$(function(){
	$('.tab-links li a').on('click', function() {
		$('.tab-links li a.active').removeClass('active');
	    $(this).addClass('active');
	    $('.tab-content.active').removeClass('active');
	    $(this.rel).addClass('active');
	    return false;
	});

	$('.schedule').slick({
	  	infinite: true,
	  	slidesToShow: 5,
	  	slidesToScroll: 1
	});
});